Template.addPost.events({

  'submit form': function(e) {
    e.preventDefault()

    var num = function() {
      var num = Math.floor(Math.random() * 4) + 1
      return num
    }

    var post = {
      titre: $(e.target).find('[name=titre]').val(),
      lecture: {
        interaction: num(),
        type: num(),
        machine: num(),
        humaine: num()
      }

    }

    Posts.insert(post)

    e.target.reset()

    //FlowRouter.go('/')

  }

})
