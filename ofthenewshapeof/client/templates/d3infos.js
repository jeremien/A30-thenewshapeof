import {Tracker} from 'meteor/tracker'

Template.infos.onRendered(function() {

  var svg,
    width = window.innerWidth,
    height = window.innerHeight,
    padding = 50,
    scaleX,
    data;

  svg = d3.select('#graph')
  .append('svg')
  .attr('width', width)
  .attr('height', height)
  .classed('container', true)

  var redraw = function(data) {

    data = _.map(data, function(num) {
      console.log(num)
      return num * 10
    })

    scaleX = d3.scale.linear()
          .domain([0, data.length])
          .range([50, width])

    scaleY = d3.scale.linear()
          .domain([0,data.length])
          .range([50, height])

    svg.selectAll('circle').data(data).enter()
    .append('circle')
    .attr('cx', padding)
    .attr('cy', function(d, i){
      console.log(scaleY(i))
      return scaleY(i);
    })
    .attr('r', function(d) {
      return d
    }).classed('item', true)

  }

  Tracker.autorun(() => {
    data = _.map(Posts.find().fetch(), (data) => {
      console.log(data)
      return data
    })

    interaction = []
    for (var i = 0; i < data.length; i++) {
      interaction.push(data[i].lecture.interaction)
    }
    redraw(interaction)
  })
})
