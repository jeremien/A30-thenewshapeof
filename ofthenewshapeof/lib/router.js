FlowRouter.route('/', {
  name:'home',
  action: function() {
    BlazeLayout.render('layout', {child: 'posts'})
  }
})

FlowRouter.route('/add', {
  name:'add',
  action: function() {
    BlazeLayout.render('layout', {child: 'addPost'})
  }
})

FlowRouter.route('/infos', {
  name:'infos',
  action: function() {
    BlazeLayout.render('layout', {child: 'infos'})
  }
})
