const express = require('express')

const app = express()

app.set('views', __dirname + '/view')
app.set('view engine', 'jade')

app.use(express.static(__dirname + '/public'))

app.get('/', (req, res) => {
    res.render('index.ejs')
})

app.get('/bar', (req, res) => {
    res.render('bar.ejs')
})

app.get('/scatter', (req, res) => {
    res.render('scatter.ejs')
})

app.get('/network', (req, res) => {
    res.render('network.ejs')
})

app.get('/bar2', (req, res) => {
    res.render('bar2.ejs')
})

app.listen(3000, () => {
  console.log('server running')
})
