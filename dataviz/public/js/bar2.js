var data;
var newData = [];

function preload() {

  var url = '../data/data.json'

  data = loadJSON(url)
  console.log(data)

}


function setup() {

  var machine = _.map(data, 'lecture.machine')
  var humain = _.map(data, 'lecture.humaine')
  var texte = _.map(data, 'titre')

  console.log(machine);

  for(var i = 0; i < machine.length; i++) {
      newData.push([machine[i],humain[i], texte[i]])
  }

  console.log(newData)

  var width = 400,
  height = 350,
  margin = 20,
  w = width - 2 * margin,
  h = height - 2 * margin

  var barWidth = (h / newData.length) * 0.8;
  var barMargin = (h / newData.length) * 0.2;

  createCanvas(width, height)

  textSize(14)


  push()
  translate(margin, margin)

  text('machine',0,0)

  for(var i=0; i < newData.length; i++) {
    push()
    fill('red')
    noStroke()
    translate(200, i* (barWidth + barMargin))
    ellipse(100, 20, newData[i][0] * 10)

    fill('black')
    textAlign(RIGHT);
    text(newData[i][2], 0, barWidth/2 + 5)

    pop()
  }

  pop()
}
