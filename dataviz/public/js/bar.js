
d3.json( '../data/data.json').then(function( data ) {

    var newData = _.map(data, 'lecture')
    //var text = _.map(data.content, 'name')

    var tab = []

    for (var i = 0; i < newData.length; i++) {
      tab.push(newData[i].interaction)
    }

    console.log(tab)

    var texte = _.map(data, 'titre')
    //console.log(texte)

    generate(tab, texte)

})

//var dataset = [30,50,12]



function generate( data, texte ) {

var w = 800;
var h = 400;


// create svg
var svg = d3.select( '#root' )
  .append( 'svg' )
  .attr( 'width', w)
  .attr( 'height', h )

// bind data
svg.selectAll( 'circle' )
    .data( data )
    .enter()
    .append( 'circle' )
    .attr( 'cx', function( d, i ) {
      return i * 150 + 100
    })
    .attr( 'cy', function( i ) {
      return h / 2
    })
    .attr( 'r', function( d ){
      return d * 10
    })
    .attr( 'fill', 'red')

svg.selectAll( 'text' )
   .data( texte )
   .enter()
   .append( 'text' )
   .text( function( d ) {
     return d
   })
   .attr( 'x', function( d,i ){
     return i * 150 + 100
   } )
   .attr( 'y', function( d ){
     return h - 20
   })
   .attr( 'font-size', 10 )
   .attr( 'text-anchor', 'middle')

svg.append("text")
   .attr("y", h / 4)
   .attr("x", w /2 )
   .attr( 'text-anchor', 'middle')
   .text("interaction")

}
