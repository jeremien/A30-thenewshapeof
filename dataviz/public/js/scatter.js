d3.json( '../data/data.json').then(function( data ) {

    var machine = _.map(data, 'machine')
    //console.log(machine)

    var humain = _.map(data, 'humain')
    //console.log(humain)

    var texte = _.map(data, 'titre')

    var newData = []
    for(var i = 0; i < 5; i++) {
        newData.push([machine[i],humain[i], texte[i]])
    }

    //console.log(newData)

    createScatter(newData, texte)

  })



function createScatter(data, text) {

  var w = 800
  var h = 400
  var p = 50

  var svg = d3.select( '#chart')
              .append( 'svg' )
              .attr( 'width', w)
              .attr( 'height', h)

  var xScale = d3.scaleLinear()
                 .domain([d3.max(data, function(d){
                   //d.reverse()
                    return d[0]
                  }),0])
                 .range([w - p, p])

  var yScale = d3.scaleLinear()
                 .domain([0, d3.max(data, function(d){
                    //d.reverse()
                    return d[0]
                  })])
                  .range([h - p, p])

  var xAxis = d3.axisBottom()
                .scale( xScale)
                .ticks(5)

  svg.append( 'g' )
     .attr( 'class', 'x-axis' )
     .attr( 'transform', 'translate(0,' + (h-p) +')')
     .call( xAxis )

  var yAxis = d3.axisLeft()
                .scale( yScale)
                .ticks(5)

  svg.append( 'g' )
     .attr( 'class', 'y-axis' )
     .attr( 'transform', 'translate(' + p +',0)')
     .call( yAxis )

  svg.selectAll('circle')
     .data( data )
     .enter()
     .append( 'circle' )
     .attr( 'cx', function( d ) {
       return xScale(d[0])
     })
     .attr( 'cy', function( d ) {
       return yScale(d[1])
     })
     .attr( 'r', 5)
     .attr('fill', 'red')
     //.attr('transform', 'translate(100,100)')

  svg.append('g')
     .selectAll( 'text' )
     .data( data )
     .enter()
     .append( 'text' )
     .text( function( d ) {
       return d[2]
     })
     .attr('x', function( d, i ){
       return xScale(d[0])
     })
     .attr('y', function( d,i ){
       return yScale(d[1])
     })
     .attr('transform', 'translate(0,-10)')
     .attr( 'text-anchor', 'middle')

  svg.append("text")
     .attr("y", h)
     .attr("x", w )
     .attr( 'text-anchor', 'middle')
     .text("humain")
     .attr('transform', 'translate(-50,-15)')

  svg.append("text")
     .attr("y", 40)
     .attr("x", 50)
     .attr( 'text-anchor', 'middle')
     .text("machine")
     .attr('transform', 'translate(0,-10)')
}
