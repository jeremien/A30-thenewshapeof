import React from 'react'
import PropTypes from 'prop-types'

import {Posts} from '../api/posts'

export default class Post extends React.Component {
  render(){
    return (
    <ul key={this.props.data._id}>
      <li><img src={this.props.data.image}/></li>
      <li>{this.props.data.titre}</li>
      <li>{this.props.data.auteur}</li>
      <li>{this.props.data.date}</li>
    </ul>
  )
  }
}

Post.propTypes = {
  data: PropTypes.object.isRequired
}
