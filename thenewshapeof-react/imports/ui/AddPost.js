import React from 'react'

import {Posts} from '../api/posts'

export default class AddPost extends React.Component {

  handleSubmit(e) {
    e.preventDefault()

    let titre = e.target.titre.value
    let auteur = e.target.auteur.value
    let date = e.target.date.value
    let image = e.target.image.value

    Posts.insert({titre, auteur, date, image})

    e.target.titre.value = ''
    e.target.auteur.value = ''
    e.target.date.value = ''
    e.target.image.value = ''
  }

  render() {
    return (<div>
      <form onSubmit={this.handleSubmit}>
        <input type="text" name="titre" placeholder="titre"/>
        <input type="text" name="auteur" placeholder="auteur"/>
        <input type="text" name="date" placeholder="date"/>
        <input type="text" name="image" placeholder="image"/>

        <button>Add</button>
      </form>
    </div>)
  }
}
