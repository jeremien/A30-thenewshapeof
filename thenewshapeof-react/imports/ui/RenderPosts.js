import React from 'react'
import PropTypes from 'prop-types'

import {Posts} from '../api/posts'
import Post from './Post'


export default class RenderPosts extends React.Component {

  renderPosts() {
    return this.props.datas.map(function(data) {
      return <Post key={data._id} data={data}/>
    })
  }

  render() {
    return (
      <div>
        {this.renderPosts()}
      </div>
    )
  }
}

RenderPosts.propTypes = {
  datas:PropTypes.array.isRequired
}
