import {Meteor} from 'meteor/meteor'
import {Tracker} from 'meteor/tracker'

import React from 'react'
import ReactDOM from 'react-dom'
import { Router, Route, browserHistory } from 'react-router'


// import {Posts} from './../imports/api/posts'

import ListPosts from './../imports/ui/ListPosts'

// import AddPost from './../imports/ui/AddPost'
// import RenderPosts from './../imports/ui/RenderPosts'

const routes = {
  <Router >
    <Route path="/" component={ ListPosts }/>
  </Router>
}


Meteor.startup(()=> {

  // Tracker.autorun(function() {
  //   // const posts = Posts.find().fetch()
  //   // console.log(posts)
  //
  //   // let jsx = (<div>
  //   //
  //   //
  //   //   {/* <AddPost/>
  //   //   <RenderPosts datas={posts}/> */}
  //   //
  //   // </div>)
  //
  // })

  ReactDOM.render(routes, document.getElementById('app'))


})
