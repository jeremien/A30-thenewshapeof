import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import 'normalize.css/normalize.css';
import './styles/styles.scss';

import data from '../public/data.json';

import App from './components/App';

ReactDOM.render(
<BrowserRouter>
    <App data={data} />
</BrowserRouter>
, document.getElementById('app'));