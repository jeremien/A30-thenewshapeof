import React from 'react';
import _ from 'lodash';

import DoughnutGraph from './graph/DoughnutGraph';
import BarGraph from './graph/BarGraph';
import LineGraph from './graph/LineGraph';
import ScatterGraph from './graph/ScatterGraph';
import RadarGraph from './graph/RadarGraph';
import PolarGraph from './graph/PolarGraph';

class Title extends React.Component {

    render() {

        let id = this.props.match.params.id;

        let dataset = _.filter(this.props.data, (item) => {
            return item._id === id;
        });

        // infos
        let infos = {};

        const infoData = dataset.map((d) => {
            infos = {
                titre: d.titre,
                date: d.date,
                auteur: d.auteur
            }
        });

        // datas
        let labels = [];
        let data = [];
        let colors = ['rgba(255,99,132,1)', 'rgba(255,99,132,0.8)', 'rgba(255,99,132,0.5)', 'rgba(255,99,132,0.2)'];

        dataset.map((item, key) => {
            _.forOwn(item.lecture, (value, key) => {
                labels.push(value.titre)
                data.push(value.value)
            })
        })

        labels = _.compact(labels);
        data = _.compact(data);

        let datax = data.map((x) => {
            return x;
        });

        let dataxy = data.map((x, y) => {
            return {
                x: y,
                y: x
            }
        });

        console.log(labels, datax, dataxy);

        return (
            <div className="infos">
                <p>{infos.titre}</p>
                <p>{infos.date}</p>
                <p>{infos.auteur}</p>

                <div className="container">
                <DoughnutGraph labels={labels} data={datax} colors={colors} />
                <BarGraph labels={labels} data={datax} colors={colors} legend='lecture' />
                <LineGraph labels={labels} data={datax} colors={colors} />
                <ScatterGraph labels={labels} data={dataxy} colors={colors} />
                <RadarGraph labels={labels} data={datax} colors={colors} />
                <PolarGraph labels={labels} data={datax} colors={colors}/>
                </div>
            </div>
        )

    }

}

export default Title;