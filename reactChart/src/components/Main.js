import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Home from './Home';
import Title from './Title';
import Notion from './Notion';


const Main = (props) => {

    const data = props.data;

    return (

        <main>
            <Switch>
                <Route exact path='/' render={(props) => <Home {...props} data={data} /> } />
                <Route path='/title/:id' render={(props) => <Title {...props} data={data} /> } />
                <Route path='/:notion' render={(props) => <Notion {...props} data={data} /> } />
            </Switch>
        </main>

    )


}

export default Main;

