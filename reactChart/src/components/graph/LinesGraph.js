import React from 'react';
import { Line } from 'react-chartjs-2';


class LinesGraph extends React.Component {

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }


    handleClick(e) {
        console.log(e[0])
    }

    render() {

        const data = this.props.data;

        const options = {
            responsive: true,
            layout : {
                padding : {
                    left: 20,
                    right: 20,
                    top: 20,
                    bottom: 20
                }
            },
            legend: {
                display: true
            },
            scales: {
                yAxes: [{
                    type: 'linear',
                    ticks: {
                        suggestedMin: 0,
                        suggestedMax: 5,
                        stepSize: 1
                    }
                }]
            }

        }

        return (

            <div className="item">
                <Line 
                    data={data} 
                    options={options}
                    getElementAtEvent={this.handleClick} 
                />
            </div>
        )

    }
}

export default LinesGraph;