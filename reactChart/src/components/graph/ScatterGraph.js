import React from 'react';
import { Scatter } from 'react-chartjs-2';

class ScatterGraph extends React.Component {

    render() {

        const data = {
            labels: this.props.labels,
            datasets: [
            {
                label: 'Lecture',
                fill: true,
                backgroundColor: this.props.colors[0],
                pointBorderColor: this.props.colors[0],
                pointBackgroundColor: '#fff',
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: this.props.colors[0],
                pointHoverBorderColor: this.props.colors[0],
                pointHoverBorderWidth: 2,
                pointRadius: 5,
                pointHitRadius: 10,
                data: this.props.data
                }
            ]
        };

        const options = {
            responsive: true,
            layout : {
                padding : {
                    left: 20,
                    right: 20,
                    top: 20,
                    bottom: 20
                }
            },
            title : {
                display: true,
                text: 'lecture'
            },
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    type: 'linear',
                    ticks: {
                        suggestedMin: 0,
                        suggestedMax: 5,
                        stepSize: 1
                    }
                }],
                xAxes: [{
                    type: 'category',
                    labels: this.props.labels
                }]
            }

        }

        return (

            <div className="item">
                <Scatter data={data} options={options} />
            </div>

        )
    }
}

export default ScatterGraph;