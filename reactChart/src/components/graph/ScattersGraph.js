import React from 'react';
import { Scatter } from 'react-chartjs-2';

class ScattersGraph extends React.Component {

    constructor(props) {
        super(props);

        this.handleClick = this.handleClick.bind(this);

    }

    handleClick(e) {
        console.log(e[0])
    }

    render() {


        const data = this.props.data;


        const options = {
            maintainAspectRatio: false,
            responsive: true,
            layout : {
                padding : {
                    left: 20,
                    right: 20,
                    top: 20,
                    bottom: 20
                }
            },
            legend: {
                display: true
            },
            scales: {
                yAxes: [{
                    type: 'linear',
                    ticks: {
                        suggestedMin: 0,
                        suggestedMax: 5,
                        stepSize: 1
                    }
                }],
                xAxes: [{
                    type: 'category',
                    labels: this.props.labels
                }]
            }

        }

        return (

            <div className="item">
                <Scatter 
                    data={data} 
                    options={options} 
                    width={500} 
                    height={500} 
                    getElementAtEvent={this.handleClick} 
                />
            </div>

        )
    }
}

export default ScattersGraph;