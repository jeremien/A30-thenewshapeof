import React from 'react';
import { Radar } from 'react-chartjs-2';


class RadarsGraph extends React.Component {

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(e) {
        console.log(e[0])
    }

    render() {

        const data = this.props.data;

        // const data = {
        //     labels: ['1','2','3'],
        //     datasets: [
        //         {
        //             label: 'titre',
        //             backgroundColor: 'red',
        //             borderColor: 'red',
        //             data: [2,4,5]
        //         }
        //     ]
        // };

        const options = {
            responsive: true,
            layout : {
                padding : {
                    left: 20,
                    right: 0,
                    top: 0,
                    bottom: 0
                }
            },
            legend: {
                display: true
            },
            scale : {
                ticks : {
                    stepSize: 1
                }
            }

        }

        return (

            <div className="item">
                <Radar 
                    data={data} 
                    options={options}
                    getElementAtEvent={this.handleClick} 
                />
            </div>

        )
    }

}

export default RadarsGraph;