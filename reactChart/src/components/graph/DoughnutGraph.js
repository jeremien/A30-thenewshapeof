import React from 'react';

import { Doughnut } from 'react-chartjs-2';


class DoughnutGraph extends React.Component {

    render() {

		const data = {
			labels: this.props.labels,
			datasets: [{
				data: this.props.data,
                backgroundColor: this.props.colors,
                hoverBackgroundColor: this.props.colors[1]

			}]
        };
        
        const options = {
            responsive: true,
            layout : {
                padding : {
                    left: 20,
                    right: 0,
                    top: 0,
                    bottom: 0
                }
            },
            title : {
                display: true,
                text: 'lecture'
            },
            legend: {
                display: true
            }

        }

        return (
            <div className="item">
                <Doughnut data={data} options={options} />
            </div>
        )

    }


}

export default DoughnutGraph;