import React from 'react';
import { Bar } from 'react-chartjs-2';
import { withRouter } from 'react-router-dom';

class BarGraph extends React.Component {

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(e) {
        // console.log(e[0])
        // console.log(e[0]._model.label)

        let dataset = _.filter(this.props.dataGlobal, (item) => {
            return item.titre === e[0]._model.label;
        });

        let id = dataset.map((i) => {
            return i._id;
        })

        console.log(id[0])
        this.props.history.push(`/title/${id[0]}`)
    }

    render() {

        // console.log(this.props.dataGlobal)

        const data = {
            labels: this.props.labels,
            datasets: [
              { 
                label: 'lecture',
                backgroundColor: this.props.colors[0],
                borderWidth: 0,
                hoverBackgroundColor: this.props.colors[1],
                data: this.props.data
              }
            ]
          };

        const options = {
            responsive: true,
            layout : {
                padding : {
                    left: 20,
                    right: 20,
                    top: 20,
                    bottom: 20
                }
            },
            title : {
                display: true,
                text: this.props.legend
            },
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    type: 'linear',
                    ticks: {
                        suggestedMin: 0,
                        suggestedMax: 5,
                        stepSize: 1
                    }
                }],
                xAxes: [{
                    type: 'category',
                    labels: this.props.labels
                }]
            }

        }

        return (
            <div className="item">
                <Bar
                    data={data}
                    width={300}
                    height={300}
                    options={options}
                    ref={'bar'}
                    // onElementsClick={(e) => console.log(this.props.labels)}
                    // getElementsAtEvent={(e) => console.log(this.props)}
                    getElementAtEvent={this.handleClick}
                    // getDatasetAtEvent={(e) => console.log(e)}
                />
            </div>
        )
    }
}

export default withRouter(BarGraph);