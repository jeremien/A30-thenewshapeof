import React from 'react';
import { Line } from 'react-chartjs-2';


class LineGraph extends React.Component {

    render() {

        const data = {
            labels: this.props.labels,
            datasets: [
              {
                label: 'lecture',
                fill: false,
                lineTension: 0,
                backgroundColor: this.props.colors[0],
                borderColor: this.props.colors[0],
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: this.props.colors[0],
                pointBackgroundColor: '#fff',
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: this.props.colors[0],
                pointHoverBorderColor: this.props.colors[0],
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.props.data
            }
            ]
        };

        const options = {
            responsive: true,
            layout : {
                padding : {
                    left: 20,
                    right: 20,
                    top: 20,
                    bottom: 20
                }
            },
            title : {
                display: true,
                text: 'lecture'
            },
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    type: 'linear',
                    ticks: {
                        suggestedMin: 0,
                        suggestedMax: 5,
                        stepSize: 1
                    }
                }]
            }

        }

        return (

            <div className="item">
                <Line data={data} options={options} />
            </div>
        )

    }
}

export default LineGraph;