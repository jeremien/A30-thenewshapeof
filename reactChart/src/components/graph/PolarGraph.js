import React from 'react';
import { Polar } from 'react-chartjs-2';

class PolarGraph extends React.Component {

    render() {

        const data = {
            datasets: [{
                data: this.props.data,
                backgroundColor: this.props.colors,
                label: 'Lecture'
            }],
            labels: this.props.labels
        };

        const options = {
            responsive: true,
            layout : {
                padding : {
                    left: 20,
                    right: 20,
                    top: 20,
                    bottom: 20
                }
            },
            title : {
                display: true,
                text: 'lecture'
            },
            legend: {
                display: true
            },
            scale : {
                ticks : {
                    stepSize: 1
                }
            }

        }


        return (

            <div className="item">
                <Polar data={data} options={options}/>
            </div>

        )
    }
}

export default PolarGraph;