import React from 'react';
import { Radar } from 'react-chartjs-2';


class RadarGraph extends React.Component {

    render() {

        const data = {
            labels: this.props.labels,
            datasets: [
                {
                    label: 'lecture',
                    backgroundColor: this.props.colors[1],
                    borderColor: this.props.colors[0],
                    pointBackgroundColor: this.props.colors[1],
                    pointBorderColor: this.props.colors[1],
                    pointHoverBackgroundColor: this.props.colors[0],
                    pointHoverBorderColor: this.props.colors[0],
                    data: this.props.data
                }
            ]
        };

        const options = {
            responsive: true,
            layout : {
                padding : {
                    left: 20,
                    right: 0,
                    top: 0,
                    bottom: 0
                }
            },
            title : {
                display: true,
                text: 'lecture'
            },
            legend: {
                display: false
            },
            scale : {
                ticks : {
                    stepSize: 1
                }
            }

        }

        return (

            <div className="item">
                <Radar data={data} options={options}/>
            </div>

        )
    }

}

export default RadarGraph;