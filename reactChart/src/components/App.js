import React from 'react';

import Main from './Main';



class App extends React.Component {

    render() {

        return (
            <div className="page">

                <Main data={this.props.data} />
            
            </div>
        )
    }

}

export default App;