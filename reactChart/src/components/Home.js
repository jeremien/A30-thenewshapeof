import React from 'react';
import { Link } from 'react-router-dom';

class Home extends React.Component {


    renderListItem() {

        return this.props.data.map((item, key) => {
            // console.log(item.titre)

            // let id = item.titre.trim().toLowerCase();
            // id = id.split(' ').join('_');
            // console.log(id)

            return (
                <li key={key} >
                    <Link to={`/title/${item._id}`}>{item.titre} </Link>
                </li>
            )
        });
    }

    renderListCategorie() {

        return <Link to={`/lecture`}> lecture </Link>;
    }



    render() {


        return (
            <div>
                <ul>
                {this.renderListItem()}
                </ul>

                {this.renderListCategorie()}
            </div>
        )
    }
}

export default Home;