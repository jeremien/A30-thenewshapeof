import React from 'react';
import _ from 'lodash';

import BarGraph from './graph/BarGraph';
import LinesGraph from './graph/LinesGraph';
import RadarsGraph from './graph/RadarsGraph';
import ScattersGraph from './graph/ScattersGraph';

class Notion extends React.Component {

    render() {

        const notion = this.props.match.params.notion;

        let labels = [];
        let dataInteraction = [];
        let dataMachine = [];
        let colors = ['rgba(255,99,132,1)', 'rgba(255,99,132,0.8)', 'rgba(255,99,132,0.5)', 'rgba(255,99,132,0.2)'];

        this.props.data.map((item) => {
            labels.push(item.titre);
            _.forOwn(item.lecture.interaction, (value) => {
                dataInteraction.push(value);
            });
            _.forOwn(item.lecture.machine, (value) => {
                dataMachine.push(value);
            });
        });

        dataInteraction = _.pull(dataInteraction, 'interaction');
        dataMachine = _.pull(dataMachine, 'machine');

        // fonction generation de couleurs

        const generateColors = () => {
            let num = Math.random()*256|0;
            return `rgba(255, 100, ${num}, 0.8)`;
        }


        // data pour lines

        const dataLectureLines = {
            labels: ['interaction', 'type', 'machine', 'humaine'],
            datasets: this.props.data.map((value) => {
                let color = generateColors();
                return {
                    label: value.titre,
                    fill: false,
                    lineTension: 0,
                    backgroundColor: color,
                    borderColor: color,
                    data: [
                        value.lecture.interaction.value,
                        value.lecture.type.value,
                        value.lecture.machine.value,
                        value.lecture.humaine.value
                        ]
                }
            })
        };

        // data pour radar

        const dataLectureRadar = {
            labels: ['interaction', 'type', 'machine', 'humaine'],
            datasets: this.props.data.map((value) => {
                let color = generateColors();
                return {
                    label: value.titre,
                    backgroundColor: color,
                    borderColor: color,
                    data: [
                        value.lecture.interaction.value,
                        value.lecture.type.value,
                        value.lecture.machine.value,
                        value.lecture.humaine.value
                        ]
                }
            })
        };

        // data pour scatter

        const dataLectureScatter = {
            labels: ['interaction', 'type', 'machine', 'humaine'],
            datasets: this.props.data.map((value) => {
                let color = generateColors();
                return {
                    label: value.titre,
                    fill: true,
                    backgroundColor: color,
                    data: [
                        { y: value.lecture.interaction.value },
                        { y: value.lecture.type.value },
                        { y: value.lecture.machine.value },
                        { y: value.lecture.humaine.value }
                    ]
                }
            })
        };



        return (

            <div className="infos">
                <p>{notion}</p>

                <div className="container">
                    <BarGraph labels={labels} dataGlobal={this.props.data} data={dataInteraction} colors={colors} legend='interaction' />
                    <BarGraph labels={labels} dataGlobal={this.props.data} data={dataMachine} colors={colors} legend='machine' />

                    <LinesGraph data={dataLectureLines} />
                    <RadarsGraph data={dataLectureRadar} />

                    <ScattersGraph data={dataLectureScatter} />

                </div>

            </div>

        )
    }

}

export default Notion;