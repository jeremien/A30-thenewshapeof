DonutChart = function(_parentElement, _datas, _titre, _h, _w, _colors) {

  this.parentElement = _parentElement;
  this.datas = _datas;
  this.titre = _titre;
  this.w = _w;
  this.h = _h;
  this.colors = _colors;

  this.initVis();

}

DonutChart.prototype.initVis = function() {

  let vis = this;

  vis.margin = { left: 10, right: 10, top :10, bottom: 10};

  vis.width = vis.w - vis.margin.left - vis.margin.right;
  vis.height = vis.h - vis.margin.top - vis.margin.bottom;
  vis.radius = Math.min(vis.width, vis.height) / 2;

  //init svg
  vis.svg = d3.select(vis.parentElement)
                .append("svg")
                .attr("width", vis.width + vis.margin.left + vis.margin.right)
                .attr("height", vis.height + vis.margin.top + vis.margin.bottom);

  vis.g = vis.svg.append("g")
               .attr("transform",
               "translate(" + vis.width / 2 + "," + vis.height / 2 + ")");

           // titre
             vis.g.append("text")
                 .attr("class", "x axis-label")
                 .attr("x", "0")
                 .attr("y", "0")
                 .attr("font-size", "20px")
                 .attr("text-anchor", "middle")
                 .text(vis.titre)


 // init pie
 vis.pie = d3.pie()
               .padAngle(0.03)
               .sort(null)
               .value(function(d) { return d.data})

 vis.path = d3.arc()
                .outerRadius(vis.radius - 10)
                .innerRadius(vis.radius - 70);

 vis.label = d3.arc()
                 .outerRadius(vis.radius - 40)
                 .innerRadius(vis.radius - 40);


 vis.arc = vis.g.selectAll(".arc")
              .data(vis.pie(vis.datas))
              .enter()
              .append("g")
                 .attr("class", "arc")

       vis.arc.append("path")
          .attr("d", vis.path)
          .attr("fill", function(d, i) { return vis.colors(i)});

       vis.arc.append("text")
          .attr("transform", function(d, i) {
            return "translate(" + vis.label.centroid(d) + ")"
          })
          .attr("font-size", "10px")
          .attr("dy", "0.35em")
          .text(function(d) { return d.data.name })

}
