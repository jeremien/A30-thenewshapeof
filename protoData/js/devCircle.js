// initialisation des formats
const margin = { left: 100, right: 10, top :10, bottom: 150};

const width = 800 - margin.left - margin.right,
      height = 400 - margin.top - margin.bottom;

//init svg
const svg = d3.select("#chart-area")
              .append("svg")
              .attr("width", width + margin.left + margin.right)
              .attr("height", height + margin.top + margin.bottom);

const g = svg.append("g")
             .attr("transform",
             "translate(" + margin.left +","+ margin.top + ")");

       // titre
         g.append("text")
             .attr("class", "x axis-label")
             .attr("x", width / 2)
             .attr("y", height + 150)
             .attr("font-size", "20px")
             .attr("text-anchor", "middle")
             .text("interaction")

// lecture
d3.json("data/data.json", function(data){

    // traitement des données
    const lecture = data.map(function(d) {
      // console.log(d)
      let obj = {
        name: d.titre,
        interaction: d["lecture"].interaction
      }
      return obj;
    })

    const names = lecture.map(function(d) {
      return d.name
    })

    console.log(lecture, names);



    // scales
    const y = d3.scaleLinear()
                .domain([0, 100])
                .range([0, height]);


    const x = d3.scaleBand()
                .domain(names)
                .range([0, width])
                .paddingInner(0.5)
                .paddingOuter(0.5)
    // init axis
    const xAxisCall = d3.axisBottom(x);
    const yAxisCall = d3.axisLeft(y)
                        .ticks(5);

    // attach axis au groupe
    g.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0, " + height + ")")
        .call(xAxisCall)
        .selectAll("text")
            .attr("y", "10")
            .attr("x", "-5")
            .attr("text-anchor", "end")
            .attr("transform", "rotate(-40)")

    g.append("g")
        .attr("class", "y axis")
        .call(yAxisCall)


    // attache les visualisations
    const circle = g.selectAll("circle")
                    .data(lecture);

              circle.enter()
                    .append("circle")
                      .attr("transform", "translate(0, 0)")
                      .attr("cx", function(d, i){
                        return x(d.name)
                      })
                      .attr("cy", function(d) {
                        return y(d.interaction)
                      })
                      .attr("r", 10);
})
