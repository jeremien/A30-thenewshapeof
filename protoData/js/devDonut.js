// initialisation des formats
const margin = { left: 10, right: 10, top :10, bottom: 10};

const width = 400 - margin.left - margin.right,
      height = 400 - margin.top - margin.bottom;

const radius = Math.min(width, height) / 2;

//init svg
const svg = d3.select("#chart-area")
              .append("svg")
              .attr("width", width + margin.left + margin.right)
              .attr("height", height + margin.top + margin.bottom);

const g = svg.append("g")
             .attr("transform",
             "translate(" + width / 2 + "," + height / 2 + ")");

         // titre
           g.append("text")
               .attr("class", "x axis-label")
               .attr("x", "0")
               .attr("y", "0")
               .attr("font-size", "20px")
               .attr("text-anchor", "middle")
               .text("interaction")

// color
var color = d3.scaleOrdinal(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);


// init pie
const pie = d3.pie()
              .padAngle(0.03)
              .sort(null)
              .value(function(d) { return d.interaction})

const path = d3.arc()
               .outerRadius(radius - 10)
               .innerRadius(radius - 70);

const label = d3.arc()
                .outerRadius(radius - 40)
                .innerRadius(radius - 40);

// lecture
d3.json("data/data.json", function(data){

    // traitement des données
    const lecture = data.map(function(d) {
      // console.log(d)
      let obj = {
        name: d.titre,
        interaction: d["lecture"].interaction
      }
      return obj;
    })

    console.log(pie(lecture))

    const arc = g.selectAll(".arc")
                 .data(pie(lecture))
                 .enter()
                 .append("g")
                    .attr("class", "arc")

          arc.append("path")
             .attr("d", path)
             .attr("fill", function(d, i) {
               return color(i)
             });

          arc.append("text")
             .attr("transform", function(d, i) {
               return "translate(" + label.centroid(d) + ")"
             })
             .attr("font-size", "10px")
             .attr("dy", "0.35em")
             .text(function(d) { return d.data.name })
})
