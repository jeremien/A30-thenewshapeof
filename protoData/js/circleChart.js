CircleChart = function(_parentElement, _datas, _names, _titre, _h, _w, _colors) {

  // initialisation du contructeur
  this.parentElement = _parentElement;
  this.datas = _datas;
  this.names = _names;
  this.titre = _titre;
  this.w = _w;
  this.h = _h;
  this.colors = _colors;

  this.initVis();

};

CircleChart.prototype.initVis = function() {

  let vis = this;

  // initialisation des formats
   vis.margin = { left: 100, right: 10, top :10, bottom: 150};

   vis.width = vis.w - vis.margin.left - vis.margin.right;
   vis.height = vis.h - vis.margin.top - vis.margin.bottom;

  //init svg
   vis.svg = d3.select(vis.parentElement)
                .append("svg")
                .attr("width", vis.width + vis.margin.left + vis.margin.right)
                .attr("height", vis.height + vis.margin.top + vis.margin.bottom);

  vis.g = vis.svg.append("g")
               .attr("transform",
               "translate(" + vis.margin.left +","+ vis.margin.top + ")");

         // titre
           vis.g.append("text")
               .attr("class", "x axis-label")
               .attr("x", vis.width / 2)
               .attr("y", vis.height + 100)
               .attr("font-size", "20px")
               .attr("text-anchor", "middle")
               .text(vis.titre)

   // scales
   vis.y = d3.scaleLinear()
               .domain([0, 100])
               .range([vis.height,0 ]);


   vis.x = d3.scaleBand()
               .domain(vis.names)
               .range([0, vis.width])
               .paddingInner(0.5)
               .paddingOuter(0.5)
   // init axis
   vis.xAxisCall = d3.axisBottom(vis.x);
   vis.yAxisCall = d3.axisLeft(vis.y)
                       .ticks(5);

   // attach axis au groupe
   vis.g.append("g")
       .attr("class", "x axis")
       .attr("transform", "translate(0, " + vis.height + ")")
       .call(vis.xAxisCall)
       .selectAll("text")
           .attr("y", "10")
           .attr("x", "-5")
           .attr("text-anchor", "end")
           .attr("transform", "rotate(-40)")

   vis.g.append("g")
       .attr("class", "y axis")
       .call(vis.yAxisCall)

   // attache les visualisations
  vis.circle = vis.g.selectAll("circle")
                   .data(vis.datas);

             vis.circle.enter()
                   .append("circle")
                     .attr("transform", "translate(0, 0)")
                     .attr("cx", function(d, i){
                       return vis.x(d.name)
                     })
                     .attr("cy", function(d) {
                       return vis.y(d.data)
                     })
                     .attr("r", 10)
                     .attr("fill", function(d, i) { return vis.colors(i)});
}
