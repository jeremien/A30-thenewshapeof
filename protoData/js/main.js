let names;
let circleChart;
let donutChart;
// color
// const colors = d3.scaleOrdinal(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);
const colors = d3.scaleOrdinal(["blue", "red"]);

// format
let height = 400;

// lecture
d3.json("data/data.json", function(data){

    /*---------------------
    lecture : interaction
    -----------------------*/
    // objet donut
    donutChart = new DonutChart("#chart-area1", data.map(function(d) {
            return { name: d.titre, data: d["lecture"].interaction }
          }), "interaction", height, 400, colors)

    // objet circle
    circleChart = new CircleChart("#chart-area1",
            data.map(function(d) {
            return { name: d.titre, data: d["lecture"].interaction }
            }),
            data.map(function(d) { return d.titre }),
            "", height, 800, colors);


    /*---------------------
    lecture : type
    -----------------------*/
    // objet donut
    donutChart = new DonutChart("#chart-area2", data.map(function(d) {
            return { name: d.titre, data: d["lecture"].type }
          }), "type", height, 400, colors)

    // objet circle
    circleChart = new CircleChart("#chart-area2",
            data.map(function(d) {
            return { name: d.titre, data: d["lecture"].type }
            }),
            data.map(function(d) { return d.titre }),
            "", height, 800, colors);


    /*---------------------
    lecture : machine
    -----------------------*/
    // objet donut
    donutChart = new DonutChart("#chart-area2", data.map(function(d) {
            return { name: d.titre, data: d["lecture"].machine }
          }), "machine", height, 400, colors)

    // objet circle
    circleChart = new CircleChart("#chart-area2",
            data.map(function(d) {
            return { name: d.titre, data: d["lecture"].machine }
            }),
            data.map(function(d) { return d.titre }),
            "", height, 800, colors);


    /*---------------------
    lecture : humain
    -----------------------*/
    // objet donut
    donutChart = new DonutChart("#chart-area2", data.map(function(d) {
            return { name: d.titre, data: d["lecture"].humaine }
          }), "humaine", height, 400, colors)

    // objet circle
    circleChart = new CircleChart("#chart-area2",
            data.map(function(d) {
            return { name: d.titre, data: d["lecture"].humaine }
            }),
            data.map(function(d) { return d.titre }),
            "", height, 800, colors);

})
